#!/bin/bash
set -euo pipefail

# safely get install.sh's directory (and, by proxy, the repo's directory)
MY_PATH="`dirname \"$BASH_SOURCE\"`"
MY_PATH="`( cd \"$MY_PATH\" && pwd )`"

if [[ -z "$MY_PATH" ]]; then
    printf "Something went very wrong. Cannot access install.sh's directory.\n"
    exit 1
fi

#echo "$MY_PATH"

printf "About to replace the dotfiles in \n\t\t$HOME\n"
printf "with their equivalents in \n\t\t$MY_PATH/dotfiles\n\n"
printf "Are you sure? (Type 'please yes' to continue): "
read please yes
if [[ ( ! -v please ) || "$please" != "please" || \
      ( ! -v yes    ) || "$yes"    != "yes"     ]]; then
    printf "Quitting...no changes were made.\n"
    exit 2
fi

# now thanks to directory's naming convention, we can drop in symlinks as needed
for file in `ls dotfiles`; do
    linkname="$HOME/.$file"
    target="$MY_PATH/dotfiles/$file"
    if [[ "$file" == "config" ]]; then
        continue
    fi
    if [[ -e "$linkname" || -h "$linkname" ]]; then
        printf "Deleting old dotfile: %s\n" $linkname
        rm -rf "$linkname"
    fi
    printf "Creating symlink: %s -> %s\n" $linkname $target
    ln -s "$target" "$linkname"
done

# config is an entire subdirectory, but we don't want
# to keep it all in the repo, just particular parts,
# so we individually symlink its components
for file in `ls dotfiles/config`; do
    linkname="$HOME/.config/$file"
    target="$MY_PATH/dotfiles/config/$file"
    if [[ -e "$linkname" || -h "$linkname" ]]; then
        printf "Deleting old .config entry: %s\n" $linkname
        rm -rf "$linkname"
    fi
    printf "Creating symlink: %s -> %s\n" $linkname $target
    ln -s "$target" "$linkname"
done

# make symlinks to executables required
echo "Installing required executables to ~/bin"
if [ ! -d ~/bin ]; then
    mkdir -p ~/bin
fi
for file in bin/*; do
    ln -s $file ~/bin/
done

echo "Would you like to install binary assets? (Y/n)"
read answer
if [[ -z "$answer" || $answer =~ "y" || $answer =~ "Y" ]]; then
    mkdir -p ~/Pictures/Wallpapers
    cp assets/yosemite-winter-1.jpg ~/Pictures/Wallpapers/
fi

echo "Would you like to install dependencies? (Y/n)"
read answer
if [[ -z "$answer" || $answer =~ "y" || $answer =~ "Y" ]]; then
    sudo apt-get install mpd
    sudo update-rc.d mpd disable
    sudo service mpd stop
    sudo apt-get install ncmpcpp
    sudo apt-get install tmux
    sudo apt-get install xclip
    sudo apt-get install vim-gtk
    sudo apt-get install sox # for "play" command to give beeps
    sudo apt-get install feh # for setting wallpaper
    # libraries for compiling xmonad from source
    sudo apt-get install libxi-dev libxine2-dev \
                         libxinerama-dev libx11-dev \
                         libxrandr-dev libxrandr2 libxinerama1 \
                         libxext6 libxext-dev libx11-6
    curl -sSL https://get.haskellstack.org/ | sh
    stack update
    stack install xmonad xmobar dmenu yeganesh # stack compiles these guys
    sudo apt-get install suckless-tools # wmname, dmenu, etc
    sudo apt-get install xmonad # to get lightdm/gnome integration with xmonad
fi

echo "To get vim working, install plugins using :PlugInstall"
echo "To get tmux working, install plugins using C-q I"

