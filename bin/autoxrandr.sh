    # xrandr's output has resolutions, one on each line, after the "connected" line
    # so we grep for the line following the connected line with --after-context
    # and try to match the available resolution

# first, we try to check if we're on a known desktop, with fixed monitors
if [[ `hostname` = "AJS18" ]]; then
    # echo "ajs"
    xrandr --output VIRTUAL1 --off --output DP2 --off --output DP1 --off --output HDMI2 --mode 1920x1200 --pos 1050x480 --rotate normal --output HDMI1 --off --output VGA1 --mode 1680x1050 --pos 0x0 --rotate left
# and if we're not, we got with the catch-all setup designed to work well with
# my laptops
elif [[ `hostname` = "debian-xps13" ]]; then
    # echo "xps"
    if xrandr -q | grep -i --after-context=2 'DP1 connected' | grep 1920x1200; then
        # echo "monitor"
        xrandr --output VIRTUAL1 --off --output DP1 --mode 1920x1200 --pos 1920x0 --rotate normal --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP2 --off
    else
        # echo "default"
        xrandr --output VIRTUAL1 --off --output DP1 --off --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP2 --off
    fi
else
    # echo "lenovo"
    # hdmi options, prefer larger
    if xrandr -q | grep -i --after-context=1 'HDMI1 connected' | grep 2560x1440; then
        xrandr --output HDMI1 --mode 2560x1440 --pos 0x0 --rotate normal --output VIRTUAL1 --off --output eDP1 --mode 1920x1080 --pos 2560x360 --rotate normal --output VGA1 --off
    elif xrandr -q | grep -i --after-context=1 'HDMI1 connected' | grep 1920x1080; then
        xrandr --output HDMI1 --mode 1920x1080 --pos 0x0 --rotate normal --output eDP1 --mode 1920x1080 --pos 1920x0 --rotate normal --output VGA1 --off
    else
        xrandr --output HDMI1 --off --output eDP1 --mode 1920x1080 --pos 0x0 --rotate normal --output VGA1 --off
    fi
fi

killall stalonetray && stalonetray &

feh --bg-fill /home/bbeltr1/Pictures/Wallpapers/yosemite-winter-1.jpg
